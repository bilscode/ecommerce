<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_user');
            $table->string('id_invoice')->unique();
            $table->string('name');
            $table->string('phone');
            $table->text('address');
            $table->integer('zip_code');
            $table->decimal('amount', 10, 2);
            $table->enum('payment_method', ['COD','BANK_BNI','BANK_BCA','BANK_BRI','BANK_MANDIRI'])->default('COD');
            $table->enum('status', ['PENDING', 'PROCESS', 'DELIVERY','DONE'])->default('PROCESS');
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
