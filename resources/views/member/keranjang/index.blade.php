@extends('layouts.master')

@section('title')
Keranjang
@endsection

@section('content')
<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-data['items']-center py-4">
                <div class="col-lg-8 col-6">
                    <h6 class="h2 d-inline-block text-white mb-0">Keranjang</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.__alert')
        </div>
        @if (count(App\Helpers\Helpers::listCart()) == 0)
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3>Tidak ada pesanan</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-8 col-xs-12">
            @foreach (App\Helpers\Helpers::listCart() as $cartsList)
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <img class="card-img-top"
                                src="{{asset('storage/barang/'.$cartsList['id'].'/'.$cartsList['image'])}}"
                                alt="{{$cartsList['name']}}">
                        </div>
                        <div class="col-5">
                            <a href="{{route('memberHome.detail', ['id'=> $cartsList['id']])}}">
                                <h2 class="mb-0">{{$cartsList['name']}}</h2>
                            </a>
                            <h4 class="text-red">{{\App\Helpers\Helpers::formatCurrency($cartsList['price'],'Rp')}}</h4>
                            <p>{{ $cartsList['description'] }}</p>
                        </div>
                        <div class="col-4 text-right">
                            @if ($cartsList['stock'] >= $cartsList['total'])
                            <form action="{{route('memberKeranjang.save', ['id' => $cartsList['id']])}}" method="get"
                                class="mb-2">
                                <div class="input-group">
                                    <input class="form-control" type="number" value="{{$cartsList['total']}}"
                                        name="jumlah">
                                    <button class="btn btn-outline-success ml-1" type="submit"><i
                                            class="fas fa-save"></i></button>
                                </div>
                            </form>
                            @else
                            <button class="btn btn-seccondary btn-block">Stok Habis</button>
                            @endif
                            <form action="{{route('memberKeranjang.delete', ['id' => $cartsList['id']])}}" method="get">
                                <button class="btn btn-danger btn-block" type="submit"><i
                                        class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- Basic with card title -->
        </div>
        <div class="col-lg-4 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-0">Ringkasan Belanja</h4>
                    <hr class="mb-2 mt-2">
                    <div class="row">
                        <div class="col-6">
                            <p class="text-muted">Total Harga</p>
                        </div>
                        <div class="col-6 pl-0 text-right">
                            <h4 class="text-red">
                                {!!\App\Helpers\Helpers::formatCurrency(\App\Helpers\Helpers::summaryPrice() ,'Rp')!!}
                            </h4>
                        </div>
                        <div class="col-12">
                            <a href="{{route('memberPembayaran')}}" class="btn btn-block btn-outline-danger"><i class="fas fa-money-bill"></i>
                                Beli
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-0"><i class="fas fa-phone text-red"></i>&nbsp;&nbsp;Kontak Penjual</h4>
                    <hr class="mb-2 mt-2">
                    <div class="row text-center">
                        <div class="col-lg-12 col-xs-12">
                            <p class="mb-1 mt-2"> SMS/WA </p>
                        </div>
                        <div class="col-lg-12 col-xs-12">
                            <p class="h3">081228839016 (Endah Tyas Palupi) </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
