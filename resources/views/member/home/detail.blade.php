@extends('layouts.master')

@section('title')
Detail
@endsection

@section('content')
<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-data['items']-center py-4">
                <div class="col-lg-8 col-6">
                    <h6 class="h2 d-inline-block text-white mb-0">Detail Barang</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">

    <div class="row">
        <div class="col-lg-12">
            @include('layouts.__alert')
        </div>
        <div class="col-12">
            <div class="card">
                <form action="{{route('adminDashboard.addCart',['id' => $data['items']['id']])}}" method="GET">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <img class="card-img-top" src="{{asset('storage/barang/'.$data['items']['id'].'/'.$data['items']['image'])}}" alt="{{$data['items']['name']}}">
                            </div>
                            <div class="col-1">&nbsp;</div>
                            <div class="col-7">
                                <h2 class="mt-5 mb-0">{{$data['items']['name']}}</h2>
                                <hr class="mb-3 mt-3">
                                <div class="row">
                                    <div class="col-3">
                                        <h4>Harga</h4>
                                    </div>
                                    <div class="col-9">
                                        <h1 class="text-red">{{\App\Helpers\Helpers::formatCurrency($data['items']['price'],'Rp')}}</h1>
                                    </div>
                                </div>
                                <hr class="mb-3 mt-3">
                                <div class="row">
                                    <div class="col-3">
                                        <h4>Stok</h4>
                                    </div>
                                    <div class="col-9">
                                        <p>{{$data['items']['stock']}}</p>
                                    </div>
                                </div>
                                <hr class="mb-3 mt-3">
                                @if ($data['items']['stock'] != 0)
                                <div class="row">
                                    <div class="col-3">
                                        <h4>Jumlah</h4>
                                    </div>
                                    <div class="col-9">
                                        <input class="form-control col-4" type="number" value="1" name="jumlah">
                                    </div>
                                </div>
                                <hr class="mb-3 mt-3">
                                @endif
                                <div class="row">
                                    <div class="col-3">
                                        <h4>Deskripsi</h4>
                                    </div>
                                    <div class="col-9">
                                        <p>{{$data['items']['description']}}</p>
                                    </div>
                                </div>
                                <hr class="mb-3 mt-3">
                                <div class="col-12 text-right">
                                    @if ($data['items']['stock'] === 0)
                                    <button class="btn btn-secondary" type="button" disabled>Stok Habis</button>
                                    @else
                                    <button class="btn btn-success" type="submit">Tambah Keranjang</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Basic with card title -->
        </div>
    </div>
</div>
@endsection
