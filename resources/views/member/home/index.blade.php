@extends('layouts.master')

@section('title')
Home
@endsection

@section('content')
<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-8 col-xs-12">
                    <h6 class="h2 d-inline-block text-white mb-0">Selamat Datang di {{env('APP_NAME')}}</h6>
                </div>
                <div class="col-lg-4 col-xs-12 text-right">
                    <form method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Cari Barang" name="q" value="{{app('request')->input('q')}}">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">

    <div class="row">
        @if ($data['count'] == 0)
        <div class="col-12">
            <div class="alert alert-warning" role="alert">
                <strong>Data Tidak ditemukan !</strong>
                <a href="{{route('memberHome')}}" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i> Reset Pencarian</a>
            </div>
        </div>
        @endif

        <div class="col-lg-12">
            @include('layouts.__alert')
        </div>

        @foreach ($data['items'] as $items)
        <div class="col-lg-3 col-sm-6">
            <div class="card">
                <a href="{{route('memberHome.detail', ['id'=> $items['id']])}}">
                    <img class="card-img-top" src="{{asset('storage/barang/'.$items['id'].'/'.$items['image'])}}" alt="{{$items['name']}}">
                </a>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <a href="{{route('memberHome.detail', ['id'=> $items['id']])}}">
                                <h4>{{$items['name']}}</h4>
                                <h5 class="text-red">{{\App\Helpers\Helpers::formatCurrency($items['price'],'Rp')}}</h5>
                            </a>
                        </div>
                        <div class="col-12">
                            @if ($items['stock'] === 0)
                            <button class="btn btn-secondary btn-block" type="button" disabled>Stok Habis</button>
                            @else
                            <a href="{{route('adminDashboard.addCart',['id' => $items['id']])}}" class="btn btn-success btn-block">Tambah Keranjang</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic with card title -->
        </div>
        @endforeach
    </div>
</div>
@endsection
