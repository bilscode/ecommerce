@extends('layouts.master')

@section('title')
Pembayaran
@endsection

@section('content')

<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-data['items']-center py-4">
                <div class="col-lg-8 col-6">
                    <h6 class="h2 d-inline-block text-white mb-0">Pembayaran</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">

    <form method="POST" autocomplete="off">
        @csrf
        <input type="hidden" name="id_invoice" value="{{\App\Helpers\Helpers::generateIdInvoice()}}">
        <div class="row">
            <div class="col-lg-12">
                @include('layouts.__alert')
            </div>
            <div class="col-lg-8 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mb-0">Detail Pengiriman</h4>
                        <hr class="mb-2 mt-2">
                        <div class="form-group">
                            <label class="form-control-label">Metode Pembayaran</label>
                            <select name="payment_method" class="form-control" onchange="BankInfo(this.value)">
                                <option value="COD" {{ old('payment_method') === 'COD' ? 'selected' : null }}>COD</option>
                                <option value="BANK_BCA" {{ old('payment_method') === 'BANK_BCA' ? 'selected' : null }}>Bank Transfer - BCA</option>
                                <option value="BANK_BRI" {{ old('payment_method') === 'BANK_BRI' ? 'selected' : null }}>Bank Transfer - BRI</option>
                                <option value="BANK_MANDIRI" {{ old('payment_method') === 'BANK_MANDIRI' ? 'selected' : null }}>Bank Transfer - MANDIRI</option>
                                <option value="BANK_BNI" {{ old('payment_method') === 'BANK_BNI' ? 'selected' : null }}>Bank Transfer - BNI</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Nama Penerima</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="cth : Bambang Priyanto"></input>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Nomor Handphone</label>
                            <input type="number" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="cth : 628123456789"></input>
                        </div>
                        <hr class="mb-2 mt-2">
                        <div class="form-group">
                            <label class="form-control-label">Alamat Pengiriman</label>
                            <textarea name="address" class="form-control" cols="30" rows="3" placeholder="Masukan alamat lengkap anda">{{ old('address') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Kode Pos</label>
                            <input type="number" name="zip_code" value="{{ old('zip_code') }}" class="form-control" placeholder="cth : 154111"></input>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mb-0">Detail Belanja</h4>
                        @foreach (App\Helpers\Helpers::listCart() as $cartsList)
                        <div class="row">
                            <div class="col-12">
                                <hr class="mb-2 mt-2">
                                <a href="{{route('memberHome.detail', ['id'=> $cartsList['id']])}}">
                                    <h4 class="mb-0">{{$cartsList['name']}}</h4>
                                </a>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted">
                                            {{\App\Helpers\Helpers::formatCurrency($cartsList['price'],'Rp')}}
                                            x
                                            {{$cartsList['total']}}
                                        </p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <h4 class="text-red">{!!\App\Helpers\Helpers::formatCurrency($cartsList['price']*$cartsList['total'],'Rp')!!}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row mb--2">
                            <div class="col-6">
                                <h4 class="text-muted">Total Tagihan ({{\App\Helpers\Helpers::totalProduct()}} Produk)</h4>
                            </div>
                            <div class="col-6 text-right">
                                <h4>
                                    {!!\App\Helpers\Helpers::formatCurrency(\App\Helpers\Helpers::summaryPrice() ,'Rp')!!}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="bankInfoBNI" style="display: none;">
                    <div class="card-body">
                        <h4 class="mb-0"><i class="ni ni-credit-card text-blue"></i>&nbsp;&nbsp;Rekening Pembayaran</h4>
                        <hr class="mb-2 mt-2">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xs-12">
                                <img class="mb-1 mt-2" src="{{ asset('assets/img/bni.png') }}" alt="Logo BNI" style="width: 50%;">
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="h3">0589068718 (BAYU BILIANTO) </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="bankInfoBRI" style="display: none;">
                    <div class="card-body">
                        <h4 class="mb-0"><i class="ni ni-credit-card text-blue"></i>&nbsp;&nbsp;Rekening Pembayaran</h4>
                        <hr class="mb-2 mt-2">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xs-12">
                                <img class="mb-1 mt-2" src="{{ asset('assets/img/bri.png') }}" alt="Logo BRI" style="width: 50%;">
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="h3">55123312123 (BAYU BILIANTO) </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="bankInfoBCA" style="display: none;">
                    <div class="card-body">
                        <h4 class="mb-0"><i class="ni ni-credit-card text-blue"></i>&nbsp;&nbsp;Rekening Pembayaran</h4>
                        <hr class="mb-2 mt-2">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xs-12">
                                <img class="mb-1 mt-2" src="{{ asset('assets/img/bca.png') }}" alt="Logo BCA" style="width: 50%;">
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="h3">557123321 (BAYU BILIANTO) </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" id="bankInfoMANDIRI" style="display: none;">
                    <div class="card-body">
                        <h4 class="mb-0"><i class="ni ni-credit-card text-blue"></i>&nbsp;&nbsp;Rekening Pembayaran</h4>
                        <hr class="mb-2 mt-2">
                        <div class="row text-center">
                            <div class="col-lg-12 col-xs-12">
                                <img class="mb-1 mt-2" src="{{ asset('assets/img/mandiri.png') }}" alt="Logo Mandiri" style="width: 50%;">
                            </div>
                            <div class="col-lg-12 col-xs-12">
                                <p class="h3">761208123 (BAYU BILIANTO) </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <p class="text-muted">Pembayaran akan dilakukan secara Cash On Delivery (COD) atau Bank Transfer, jumlah tagihan pada metode COD dapat dibayarkan kepada kurir saat pesanan sampai dirumah. Seluruh layanan mendapatkan keuntungan gratis ongkos kirim.</p>
                        <button class="btn btn-success btn-block">Proses Pesanan <i class="fas fa-arrow-right"></i></button>
                    </div>
                </div>
            </div>
            <!-- Basic with card title -->
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    var payment_method_old = '{{ old('payment_method') }}';
    if (payment_method_old === 'BANK_BNI') {
        $('#bankInfoBNI').show();
        $('#bankInfoBRI').hide();
        $('#bankInfoBCA').hide();
        $('#bankInfoMANDIRI').hide();
    } else if (payment_method_old === 'BANK_BCA'){
        $('#bankInfoBNI').hide();
        $('#bankInfoBRI').hide();
        $('#bankInfoBCA').show();
        $('#bankInfoMANDIRI').hide();
    } else if (payment_method_old === 'BANK_BRI'){
        $('#bankInfoBNI').hide();
        $('#bankInfoBRI').show();
        $('#bankInfoBCA').hide();
        $('#bankInfoMANDIRI').hide();
    } else if (payment_method_old === 'BANK_MANDIRI'){
        $('#bankInfoBNI').hide();
        $('#bankInfoBRI').hide();
        $('#bankInfoBCA').hide();
        $('#bankInfoMANDIRI').show()
    } else {
        $('#bankInfoBNI').hide();
        $('#bankInfoBRI').hide();
        $('#bankInfoBCA').hide();
        $('#bankInfoMANDIRI').hide();
    }
    function BankInfo(value) {
        if (value === 'BANK_BNI') {
            $('#bankInfoBNI').show();
            $('#bankInfoBRI').hide();
            $('#bankInfoBCA').hide();
            $('#bankInfoMANDIRI').hide();
        } else if (value === 'BANK_BCA'){
            $('#bankInfoBNI').hide();
            $('#bankInfoBRI').hide();
            $('#bankInfoBCA').show();
            $('#bankInfoMANDIRI').hide();
        } else if (value === 'BANK_BRI'){
            $('#bankInfoBNI').hide();
            $('#bankInfoBRI').show();
            $('#bankInfoBCA').hide();
            $('#bankInfoMANDIRI').hide();
        } else if (value === 'BANK_MANDIRI'){
            $('#bankInfoBNI').hide();
            $('#bankInfoBRI').hide();
            $('#bankInfoBCA').hide();
            $('#bankInfoMANDIRI').show()
        } else {
            $('#bankInfoBNI').hide();
            $('#bankInfoBRI').hide();
            $('#bankInfoBCA').hide();
            $('#bankInfoMANDIRI').hide();
        }
    }
</script>
@endsection
