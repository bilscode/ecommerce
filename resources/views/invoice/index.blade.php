@extends('layouts.master')

@section('title')
Invoice
@endsection

@section('content')
<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-items-center py-4">
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">

        <div class="col-lg-12">
            @include('layouts.__alert')
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h3>INVOICE - {{$data['invoice']['id_invoice']}}</h3>
                        </div>
                        <div class="col-6 text-right aling-middle">
                            <h5 class="text-muted">Tanggal
                                {{\App\Helpers\Helpers::formatDate($data['invoice']['created_at'])}}</h5>
                        </div>
                    </div>
                    <hr class="my-2">
                    <div class="row">
                        <div class="col-6">
                            <h4>Tujuan Pengiriman :</h4>
                            <p>{{$data['invoice']['name']}}</p>
                            <p class="mt--3">{{$data['invoice']['address']}}</p>
                            <p class="mt--3">{{$data['invoice']['phone']}}</p>
                        </div>
                        <div class="col-6">
                            <h4>Pemesan :</h4>
                            <p>{{$data['invoice']['users']['name']}}</p>
                            <p class="mt--3">{{$data['invoice']['users']['email']}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center" width="10%">Jumlah</th>
                                    <th class="text-center">Barang</th>
                                    <th class="text-right">Harga</th>
                                </tr>
                            </thead>
                            <tbody class="list">
                                @foreach ($data['order'] as $order)
                                <tr>
                                    <td class="text-center">
                                        {{$order['total']}}
                                    </td>
                                    <td class="text-center">
                                        {{$order['item']}}
                                    </td>
                                    <td class="text-right">
                                        {{\App\Helpers\Helpers::formatCurrency($order['price']*$order['total'],'Rp')}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="mt-6">&nbsp;</div>
                            <div class="mt-6">&nbsp;</div>
                            <a href="{{route('invoicePreview', ['id' => $data['invoice']['id_invoice']])}}" class="btn btn-outline-success mt-6" target="_blank">CETAK</a>
                        </div>
                        <div class="col-6">
                            <table class="table align-items-center table-flush">
                                <tr>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Ongkos Kirim</h4>
                                    </td>
                                    <td class="text-right">
                                        <p>Bebas Ongkir</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h4>Metode Pembayaran</h4>
                                    </td>
                                    <td class="text-right">
                                        @if ($data['invoice']['payment_method'] === 'BANK_BNI')
                                            <p>Bank Transfer - BNI</p>
                                            <img class="mb-1 mt-2" src="{{ asset('assets/img/bni.png') }}" alt="Logo BNI" style="width: 15%;">
                                            <p class="h3">0589068718 (BAYU BILIANTO) </p>
                                        @elseif ($data['invoice']['payment_method'] === 'BANK_BCA')
                                            <p>Bank Transfer - BCA</p>
                                            <img class="mb-1 mt-2" src="{{ asset('assets/img/bca.png') }}" alt="Logo BNI" style="width: 15%;">
                                            <p class="h3">557123321 (BAYU BILIANTO) </p>
                                        @elseif ($data['invoice']['payment_method'] === 'BANK_MANDIRI')
                                            <p>Bank Transfer - Mandiri</p>
                                            <img class="mb-1 mt-2" src="{{ asset('assets/img/mandiri.png') }}" alt="Logo BNI" style="width: 15%;">
                                            <p class="h3">761208123 (BAYU BILIANTO) </p>
                                        @elseif ($data['invoice']['payment_method'] === 'BANK_BRI')
                                            <p>Bank Transfer - BRI</p>
                                            <img class="mb-1 mt-2" src="{{ asset('assets/img/bri.png') }}" alt="Logo BNI" style="width: 15%;">
                                            <p class="h3">55123312123 (BAYU BILIANTO) </p>
                                        @else
                                            <p>Cash On Delivery (COD)</p>
                                        @endif
                                    </td>
                                </tr>
                                <td>
                                    <h4>Total Harga</h4>
                                </td>
                                <td class="text-right">
                                    <h4>{{\App\Helpers\Helpers::formatCurrency($data['invoice']['amount'],'Rp')}}</h4>
                                </td>
                            </tbody>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
