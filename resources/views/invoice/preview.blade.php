<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>{{ env('APP_NAME') }} | Invoice</title>
    <!-- Favicon -->
    <link rel="icon" href="{{asset('assets/img/brand/favicon.png')}}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}"
        type="text/css">
    @yield('css')
    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/argon.css?v=1.1.0')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
</head>
<body onload="window.print()">

<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3>INVOICE - {{$data['invoice']['id_invoice']}}</h3>
                </div>
                <div class="col-6 text-right aling-middle">
                    <h5 class="text-muted">Tanggal
                        {{\App\Helpers\Helpers::formatDate($data['invoice']['created_at'])}}</h5>
                </div>
            </div>
            <hr class="my-2">
            <div class="row">
                <div class="col-6">
                    <h4>Tujuan Pengiriman :</h4>
                    <p>{{$data['invoice']['name']}}</p>
                    <p class="mt--3">{{$data['invoice']['address']}}</p>
                    <p class="mt--3">{{$data['invoice']['phone']}}</p>
                </div>
                <div class="col-6">
                    <h4>Pemesan :</h4>
                    <p>{{$data['invoice']['users']['name']}}</p>
                    <p class="mt--3">{{$data['invoice']['users']['email']}}</p>
                </div>
            </div>
            <div class="row">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center" width="10%">Jumlah</th>
                            <th class="text-center">Barang</th>
                            <th class="text-right">Harga</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @foreach ($data['order'] as $order)
                        <tr>
                            <td class="text-center">
                                {{$order['total']}}
                            </td>
                            <td class="text-center">
                                {{$order['item']}}
                            </td>
                            <td class="text-right">
                                {{\App\Helpers\Helpers::formatCurrency($order['price']*$order['total'],'Rp')}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-6">
                    &nbsp;
                </div>
                <div class="col-6">
                    <table class="table align-items-center table-flush">
                        <tr>
                        </tr>
                        <tr>
                            <td>
                                <h4>Ongkos Kirim</h4>
                            </td>
                            <td class="text-right">
                                <p>Bebas Ongkir</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4>Metode Pembayaran</h4>
                            </td>
                            <td class="text-right">
                                @if ($data['invoice']['payment_method'] === 'BANK_BNI')
                                    <p>Bank Transfer - BNI</p>
                                    <img class="mb-1 mt-2" src="{{ asset('assets/img/bni.png') }}" alt="Logo BNI" style="width: 15%;">
                                    <p class="h3">0589068718 (BAYU BILIANTO) </p>
                                @elseif ($data['invoice']['payment_method'] === 'BANK_BCA')
                                    <p>Bank Transfer - BCA</p>
                                    <img class="mb-1 mt-2" src="{{ asset('assets/img/bca.png') }}" alt="Logo BNI" style="width: 15%;">
                                    <p class="h3">557123321 (BAYU BILIANTO) </p>
                                @elseif ($data['invoice']['payment_method'] === 'BANK_MANDIRI')
                                    <p>Bank Transfer - Mandiri</p>
                                    <img class="mb-1 mt-2" src="{{ asset('assets/img/mandiri.png') }}" alt="Logo BNI" style="width: 15%;">
                                    <p class="h3">761208123 (BAYU BILIANTO) </p>
                                @elseif ($data['invoice']['payment_method'] === 'BANK_BRI')
                                    <p>Bank Transfer - BRI</p>
                                    <img class="mb-1 mt-2" src="{{ asset('assets/img/bri.png') }}" alt="Logo BNI" style="width: 15%;">
                                    <p class="h3">55123312123 (BAYU BILIANTO) </p>
                                @else
                                    <p>Cash On Delivery (COD)</p>
                                @endif
                            </td>
                        </tr>
                        <td>
                            <h4>Total Harga</h4>
                        </td>
                        <td class="text-right">
                            <h4>{{\App\Helpers\Helpers::formatCurrency($data['invoice']['amount'],'Rp')}}</h4>
                        </td>
                    </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/argon.js?v=1.1.0')}}"></script>
<!-- Demo JS - remove this in your project -->
<script src="{{asset('assets/js/demo.min.js')}}"></script>

</body>

</html>
