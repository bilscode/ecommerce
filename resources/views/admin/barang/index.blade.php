@extends('layouts.master')

@section('title')
Barang
@endsection

@section('css')

<link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">

@endsection

@section('content')
<div class="header bg-success pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Data Barang</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('layouts.__alert')
        </div>
    </div>

    <!-- Table -->
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah-barang">
                        Tambah Barang
                    </button>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush text-center" id="datatable-buttons">
                        <thead class="thead-light">
                            <tr>
                                <th>Dibuat Pada</th>
                                <th>Nama Barang</th>
                                <th>Gambar</th>
                                <th>Harga</th>
                                <th>Stok</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['items'] as $items)
                            <tr>
                                <td class="align-middle">{!!\App\Helpers\Helpers::formatDate($items['created_at'], true)!!}</td>
                                <td class="align-middle">{{$items['name']}}</td>
                                <td class="align-middle">
                                    <a href="#" data-toggle="tooltip" data-original-title="{{$items['name']}}">
                                        <img class="image-barang "alt="{{$items['name']}}" src="{{asset('storage/barang/'.$items['id'].'/'.$items['image'])}}">
                                    </a>
                                </td>
                                <td class="align-middle">{{\App\Helpers\Helpers::formatCurrency($items['price'],'Rp')}}</td>
                                <td class="align-middle">{{$items['stock']}}</td>
                                <td class="align-middle">
                                    <a href="{{route('adminBarang.show', ['id' => $items['id']])}}" class="btn btn-icon btn-sm btn-primary" type="button" data-toggle="tooltip" data-original-title="Edit">
                                        <span class="btn-inner--icon"><i class="fas fa-pencil-alt"></i></span>
                                    </a>
                                </td>
                                <td class="align-middle">
                                    <button class="btn btn-icon btn-sm btn-danger" type="button" data-toggle="modal" data-target="#hapus-barang" onclick="HapusBarang('{{$items['id']}}');">
                                        <span class="btn-inner--icon"><i class="fas fa-trash"></i></span>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="tambah-barang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" enctype="multipart/form-data" autocomplete="off">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label">Nama Barang</label>
                        <input name="name" value="{{old('name')}}"  type="text" class="form-control" placeholder="contoh : Teh Kotak" required>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Gambar</label>
                        <div class="custom-file">
                            <input name="image" type="file" class="form-control" required>
                        </div>
                        <p class="text-danger mb-0"">Ukuran gambar harus 400x400px</p>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Harga</label>
                        <input name="price" value="{{old('price')}}"  type="number" class="form-control" placeholder="contoh : 12000" required>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Stok</label>
                        <input name="stock" value="{{old('stock')}}"  type="number" class="form-control" placeholder="contoh : 2" required>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Deskripsi</label>
                        <textarea name="description" class="form-control" rows="3" required>{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="hapus-barang" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">
        	
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Hapus Barang</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body">
            	
                <div class="py-3 text-center">
                    <i class="ni ni-bell-55 ni-3x"></i>
                    <h4 class="heading mt-4">Yakin menghapus barang ?</h4>
                    <p>Seluruh data yang terkait dengan barang tersebut akan ikut terhapus !</p>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <form action="{{route('adminBarang.destroy')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="idBarang">
                    <button type="submit" class="btn btn-white">Hapus</button>
                </form>
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Batal</button>
            </div>
            
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="{{asset('assets/vendor/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables.net-select/js/dataTables.select.min.js')}}"></script>
<script>
    function HapusBarang(id){
        $("#idBarang").val(id);
    }
</script>
@endsection
