@extends('layouts.master')

@section('title')
Barang
@endsection

@section('css')

@endsection

@section('content')
<div class="header bg-success pb-6">
    <div class="container-fluid">
        <div class="header-body">
            &nbsp;
        </div>
    </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('layouts.__alert')
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8 card-wrapper">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0">Edit Barang</h3>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data">
                        @csrf
                        <center>
                        <img style="width:200px;" src="{{asset('storage/barang/'.$data['items']['id'].'/'.$data['items']['image'])}}" alt="{{$data['items']['name']}}">
                        </center>
                        <div class="form-group">
                            <label class="form-control-label">Nama Barang</label>
                            <input name="name" value="{{$data['items']['name']}}"  type="text" class="form-control" placeholder="contoh : Teh Kotak" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Gambar</label> <span style="font-style:italic;font-size:12px;color:rebeccapurple">*tidak perlu diisi jika tidak dirubah</span>
                            <div class="custom-file">
                                <input name="image" type="file" class="form-control">
                            </div>
                            <p class="text-danger mb-0"">Ukuran gambar harus 400x400px</p>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Harga</label>
                            <input name="price" value="{{(int)$data['items']['price']}}"  type="number" class="form-control" placeholder="contoh : 12000" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Stok</label>
                            <input name="stock" value="{{$data['items']['stock']}}"  type="number" class="form-control" placeholder="contoh : 2" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Deskripsi</label>
                            <textarea name="description" class="form-control" rows="3" required>{{$data['items']['description']}}</textarea>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
@endsection

@section('js')

@endsection
