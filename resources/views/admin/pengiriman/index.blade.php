@extends('layouts.master')

@section('title')
Pengiriman
@endsection

@section('content')
<div class="header pb-2">
    <div class="container-fluid bg-success pb-6">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 d-inline-block text-white mb-0">Selamat Datang di Pengiriman</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">

    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('layouts.__alert')
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body my--4 mx--3">
                    <div class="nav-wrapper ">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-waiting-tab" data-toggle="tab"
                                    href="#tabs-waiting" role="tab" aria-controls="tabs-waiting" aria-selected="true">
                                    <i class="ni ni-time-alarm mr-2"></i>
                                    Menunggu Konfirmasi
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-process-tab" data-toggle="tab"
                                    href="#tabs-process" role="tab" aria-controls="tabs-process" aria-selected="false">
                                    <i class="ni ni-app mr-2"></i>
                                    Proses
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-delivery-tab" data-toggle="tab"
                                    href="#tabs-delivery" role="tab" aria-controls="tabs-delivery"
                                    aria-selected="false">
                                    <i class="ni ni-delivery-fast mr-2"></i>
                                    Pengiriman
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-done-tab" data-toggle="tab"
                                    href="#tabs-done" role="tab" aria-controls="tabs-done" aria-selected="false">
                                    <i class="ni ni-check-bold mr-2"></i>
                                    Selesai
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="tabs-waiting" role="tabpanel"
                                    aria-labelledby="tabs-waiting-tab">
                                    <div class="table-responsive">
                                        <table class="table table-flush text-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Tanggal Update</th>
                                                    <th>ID Invoice</th>
                                                    <th>Jumlah Produk</th>
                                                    <th>Total Harga</th>
                                                    <th>Status</th>
                                                    <th>Konfirmasi Pembayaran</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['waiting'] as $waiting)
                                                <tr>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatDate($waiting['updated_at'], true)}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <a href="{{route('invoiceIndex', ['id' => $waiting['id_invoice']])}}">
                                                            <h5 class="text-red">
                                                                {{$waiting['id_invoice']}}
                                                                <i class="fas fa-link"></i>
                                                            </h5>
                                                        </a>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::totalOrder($waiting['id_invoice'])}}
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatCurrency($waiting['amount'], 'Rp')}}
                                                    </td>
                                                    <td class="align-middle">
                                                        @if ($waiting['payment_image'])
                                                            <span class="badge badge-warning badge-lg">MENUGGU KONFIRMASI</span>
                                                        @else
                                                            <span class="badge badge-danger badge-lg">MENUGGU PEMBAYARAN</span>
                                                        @endif
                                                    </td>
                                                    <td class="align-middle">
                                                        @if ($waiting['payment_image'])
                                                        <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#konfirmasi-pembayaran" onclick="KonfirmasiPembayaran('{{$waiting['id']}}','{{asset('storage/payment/'.$waiting['id_invoice'].'/'.$waiting['payment_image'])}}');">Lihat Bukti Pembayaran <i class="ni ni-credit-card"></i></button>
                                                        @else
                                                        -
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if ($data['c_waiting'] == 0)
                                        <div class="text-center my-4">
                                            <h3>Tidak ada pesanan</h3>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tabs-process" role="tabpanel"
                                    aria-labelledby="tabs-process-tab">
                                    <div class="table-responsive">
                                        <table class="table table-flush text-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Tanggal Update</th>
                                                    <th>ID Invoice</th>
                                                    <th>Jumlah Produk</th>
                                                    <th>Total Harga</th>
                                                    <th>Status</th>
                                                    <th>Konfirmasi Pengiriman</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['process'] as $process)
                                                <tr>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatDate($process['updated_at'], true)}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <a href="{{route('invoiceIndex', ['id' => $process['id_invoice']])}}">
                                                            <h5 class="text-red">
                                                                {{$process['id_invoice']}}
                                                                <i class="fas fa-link"></i>
                                                            </h5>
                                                        </a>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::totalOrder($process['id_invoice'])}}
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatCurrency($process['amount'], 'Rp')}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <span class="badge badge-info badge-lg">PROSES</span>
                                                    </td>
                                                    <td class="align-middle">
                                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#konfirmasi-pengiriman" onclick="KirimBarang('{{$process['id']}}');">Konfirmasi <i class="ni ni-delivery-fast"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if ($data['c_process'] == 0)
                                        <div class="text-center my-4">
                                            <h3>Tidak ada pesanan</h3>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tabs-delivery" role="tabpanel"
                                    aria-labelledby="tabs-delivery-tab">
                                    <div class="table-responsive">
                                        <table class="table table-flush text-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Tanggal Update</th>
                                                    <th>ID Invoice</th>
                                                    <th>Jumlah Produk</th>
                                                    <th>Total Harga</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['delivery'] as $delivery)
                                                <tr>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatDate($delivery['updated_at'], true)}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <a href="{{route('invoiceIndex', ['id' => $delivery['id_invoice']])}}">
                                                            <h5 class="text-red">
                                                                {{$delivery['id_invoice']}}
                                                                <i class="fas fa-link"></i>
                                                            </h5>
                                                        </a>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::totalOrder($delivery['id_invoice'])}}
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatCurrency($delivery['amount'], 'Rp')}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <span class="badge badge-warning badge-lg">PENGIRIMAN</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if ($data['c_delivery'] == 0)
                                        <div class="text-center my-4">
                                            <h3>Tidak ada pesanan</h3>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tabs-done" role="tabpanel"
                                    aria-labelledby="tabs-done-tab">
                                    <div class="table-responsive">
                                        <table class="table table-flush text-center">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Tanggal Update</th>
                                                    <th>ID Invoice</th>
                                                    <th>Jumlah Produk</th>
                                                    <th>Total Harga</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($data['done'] as $done)
                                                <tr>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatDate($done['updated_at'], true)}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <a href="{{route('invoiceIndex', ['id' => $done['id_invoice']])}}">
                                                            <h5 class="text-red">
                                                                {{$done['id_invoice']}}
                                                                <i class="fas fa-link"></i>
                                                            </h5>
                                                        </a>
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::totalOrder($done['id_invoice'])}}
                                                    </td>
                                                    <td class="align-middle">
                                                        {{\App\Helpers\Helpers::formatCurrency($done['amount'], 'Rp')}}
                                                    </td>
                                                    <td class="align-middle">
                                                        <span class="badge badge-primary badge-lg">SELESAI</span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        @if ($data['c_done'] == 0)
                                        <div class="text-center my-4">
                                            <h3>Tidak ada pesanan</h3>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="konfirmasi-pembayaran" tabindex="-1" role="dialog" aria-labelledby="modal-default"
    aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Bukti Pembayaran</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <img style="width: 100%;" id="BuktiPembayaran">
            </div>
            <div class="modal-footer">
                <form action="{{route('adminPengiriman.payment')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="idInvoicePembayaran">
                    <button type="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>
                </form>
                <button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="konfirmasi-pengiriman" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-default modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-default">

            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Kirim Pesanan</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="py-3 text-center">
                    <i class="ni ni-delivery-fast ni-3x"></i>
                    <h4 class="heading mt-4">Yakin mengkonfirmasi pengiriman pesanan pelanggan ?</h4>
                </div>

            </div>

            <div class="modal-footer">
                <form action="{{route('adminPengiriman.confirm')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="idInvoice">
                    <button type="submit" class="btn btn-secondary">Konfirmasi</button>
                </form>
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Batal</button>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    function KirimBarang(id){
        $("#idInvoice").val(id);
    }
    function KonfirmasiPembayaran(id,image) {
        $("#idInvoicePembayaran").val(id);
        $("#BuktiPembayaran").attr("src",image);
    }
</script>
@endsection
