@extends('layouts.master')

@section('title')
    Dashboard
@endsection

@section('content')
<div class="header bg-success pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-6">
    <div class="d-flex justify-content-center">
        <h1>Selamat datang di dashboard Administrator {{env('APP_NAME')}}</h1>
    </div>
</div>
@endsection