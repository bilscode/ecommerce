@extends('layouts.master')

@section('title')
    Profil
@endsection

@section('content')
<div class="header bg-success pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Profil</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('layouts.__alert')
        </div>
    </div>

    <!-- Table -->
    <div class="row">
        <div class="col">
            <div class="card">

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h3>Data Diri</h3>
                            <hr class="mt-0">
                            <form method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="form-control-label">Nama</label>
                                    <input name="name" value="{{Auth::user()->name}}"  type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Email</label>
                                    <input value="{{Auth::user()->email}}"  type="text" class="form-control" readonly>
                                </div>
                                <hr>
                                <div class="text-right">
                                    <button class="btn btn-success" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-6">
                            <h3>Ubah Kata Sandi</h3>
                            <hr class="mt-0">
                            <form method="post" action="{{route('profil.password')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="form-control-label">Password Lama</label>
                                    <input name="password_old" type="password" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Password Baru</label>
                                    <input name="password" type="password" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Konfirmasi Password</label>
                                    <input name="password_confirmation" type="password" class="form-control" required>
                                </div>
                                <div class="text-right">
                                    <button class="btn btn-outline-success" type="submit">Ubah</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>
</div>
@endsection