@if ($errors->any())
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <span class="alert-inner--icon"><i class="fa fa-ban"></i></span>
    <span class="alert-inner--text"><strong>Gagal!</strong>
    @foreach ($errors->all() as $error)
        <p class="mb-0">- {{ $error }}</p>
    @endforeach 
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if (session('info'))
<div class="alert alert-info alert-dismissible fade show" role="alert">
    <span class="alert-inner--icon"><i class="fa fa-info"></i></span>
    <span class="alert-inner--text"><strong>Info!</strong> {{ session('info') }}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <span class="alert-inner--icon"><i class="fa fa-check"></i></span>
    <span class="alert-inner--text"><strong>Sukses!</strong> {{ session('success') }}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if (session('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <span class="alert-inner--icon"><i class="fa fa-ban"></i></span>
    <span class="alert-inner--text"><strong>Gagal!</strong> {{ session('danger') }}</span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif