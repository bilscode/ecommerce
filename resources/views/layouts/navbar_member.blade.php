<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="{{route('index')}}">
                {{env('APP_NAME')}}
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">

                    <li class="nav-item">
                        <a class="nav-link {{App\Helpers\Helpers::checkUrlForMenu('')}}" href="{{route('memberHome')}}">
                            <i class="ni ni-basket text-green"></i>
                            <span class="nav-link-text">Home</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{App\Helpers\Helpers::checkUrlForMenu('keranjang')}}" href="{{route('memberKeranjang')}}">
                            <i class="ni ni-cart text-blue"></i>
                            <span class="nav-link-text">Keranjang</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{App\Helpers\Helpers::checkUrlForMenu('status-pengiriman')}}" href="{{route('memberPengiriman')}}">
                            <i class="ni ni-delivery-fast text-red"></i>
                            <span class="nav-link-text">Status Pengiriman</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>