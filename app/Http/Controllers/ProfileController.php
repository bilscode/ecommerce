<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\UpdateProfilePasswordRequest;
use Hash;
use App\Helpers\Helpers;

class ProfileController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['MEMBER','ADMIN'])) {
                return $next($request);
            }
        });
    }
    
    public function index()
    {
        return view('profil/index');
    }

    public function update(Request $request)
    {
        $data = [];
        $user = auth()->user();
        $data['name'] = $request['name'];
        $userUpdate = User::find($user['id'])->update($data);
        return redirect()->back()->with('success', 'Berhasil merubah profil !');
    }

    public function updatePassword(UpdateProfilePasswordRequest $request)
    {
        $user = auth()->user();
        $userUpdate = User::find($user['id']);
        if(Hash::check($request['password_old'], $userUpdate['password'])) {
            $password = Hash::make($request['password']);
            $userUpdate->update([
                'password' => $password,
            ]);
        } else {
            return redirect()->back()->with('danger', 'Password lama anda tidak cocok !');
        }
        return redirect()->back()->with('success', 'Berhasil merubah password !');
    }
}
