<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInvoiceRequest;
use Illuminate\Http\Request;
use App\Helpers\Helpers;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Str;

class PembayaranController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['MEMBER'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        return view('member/pembayaran/index');
    }

    public function create(CreateInvoiceRequest $request)
    {
        $data = [];
        $passData = [];
        $orderList = [];
        $orders = [];
        $amount = 0;
        $user = auth()->user();
        $checkData = Invoice::where('id_invoice', $request['id_invoice'])->count();
        if ($checkData != 0) {
            abort(403,'Unauthorized Action');
        }
        $data['id_user'] = $user['id'];
        $data['id_invoice'] = $request['id_invoice'];
        $data['name'] = Str::title($request['name']);
        $data['phone'] = $request['phone'];
        $data['address'] = $request['address'];
        $data['zip_code'] = (int)$request['zip_code'];
        $data['payment_method'] = $request['payment_method'];
        if ($data['payment_method'] !== 'COD') {
            $data['status'] = 'PENDING';
        }
        $cart = Cart::where('id_user', $user['id'])->latest()->get();
        for ($i=0; $i < $cart->count(); $i++) {
            $items = Item::where('id', $cart[$i]['id_item'])->first();
            if ($items['stock'] >= $cart[$i]['total']) {
                $orderList[$i]['id_invoice'] = $data['id_invoice'];
                $orderList[$i]['item'] = $items['name'];
                $orderList[$i]['price'] = $items['price'];
                $orderList[$i]['total'] = $cart[$i]['total'];
                $amount += $orderList[$i]['price'] * $orderList[$i]['total'];
                $orders[] = $orderList[$i];
                $orderList = null;
                $remainingStock = $items['stock'] - $cart[$i]['total'];
                $items->update(['stock' => $remainingStock]);
                Cart::where(['id_user'=>$user['id'],'id_item'=>$items['id']])->first()->delete();
            }
        }
        $data['amount'] = (int)$amount;
        $invoice = Invoice::create($data);
        foreach ($orders as $order) {
            Order::create($order);
        }
        if ($data['payment_method'] !== 'COD') {
            $passData['status'] = 'sedang menunggu proses konfirmasi pembayaran';
        } else {
            $passData['status'] = 'sedang dalam tahap pemrosesan';
        }
        $passData['id_invoice'] = $data['id_invoice'];
        User::find($user->id)->notify(new \App\Notifications\SendUpdateProgressNotifications($passData));
        return redirect()->route('invoiceIndex', ['id' => $data['id_invoice']])->with('success','Transaksi berhasil !');
    }
}
