<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Item;

class KeranjangController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['MEMBER'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        return view('member/keranjang/index');
    }

    public function save($id, Request $request)
    {
        $request->validate([
            'jumlah' => 'nullable|numeric|min:1',
        ]);
        $user = auth()->user();
        $data['total'] = $request['jumlah'];
        $item = Item::find($id);
        if ($item['stock'] < $data['total']) {
            return redirect()->back()->with('danger', 'Tidak dapat menambahkan ke keranjang dikarenakan stok tidak cukup !');
        }
        $cartUpdate = Cart::where(['id_user' => $user['id'],'id_item' => $id])->first();
        $cartUpdate->update($data);
        return redirect()->back()->with('success', 'Berhasil merubah keranjang !');
    }

    public function delete($id)
    {
        $user = auth()->user();
        $cartDelete = Cart::where(['id_user' => $user['id'],'id_item' => $id])->delete();
        return redirect()->back()->with('success', 'Berhasil mengapus keranjang !');
    }
}
