<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\User;
use App\Http\Requests\UploadPaymentImageRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PengirimanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['MEMBER'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        $data = [];
        $user = auth()->user();
        $waiting = Invoice::where(['id_user' => $user['id'], 'status' => 'PENDING'])->latest()->get();
        $process = Invoice::where(['id_user' => $user['id'], 'status' => 'PROCESS'])->latest()->get();
        $delivery = Invoice::where(['id_user' => $user['id'], 'status' => 'DELIVERY'])->latest()->get();
        $done = Invoice::where(['id_user' => $user['id'], 'status' => 'DONE'])->latest()->get();
        $data['waiting'] = $waiting;
        $data['process'] = $process;
        $data['delivery'] = $delivery;
        $data['done'] = $done;
        $data['c_waiting'] = count($waiting);
        $data['c_process'] = count($process);
        $data['c_delivery'] = count($delivery);
        $data['c_done'] = count($done);
        return view('member/pengiriman/index')->with('data' ,$data);
    }

    public function confirm(Request $request)
    {
        $user = auth()->user();
        $confirm = Invoice::where([
            'id' => $request['id'],
            'id_user' => $user['id'],
        ])->first();
        if ($confirm == null) {
            abort(403);
        }
        $confirm->update(['status' => 'DONE']);
        $passData['status'] = 'sudah diterima dan transaksi anda telah selesai';
        $passData['id_invoice'] = $confirm['id_invoice'];
        User::find($confirm['id_user'])->notify(new \App\Notifications\SendUpdateProgressNotifications($passData));
        return redirect()->back()->with('success', 'Berhasil menerima pesanan !');
    }

    public function payment(UploadPaymentImageRequest $request)
    {
        $user = auth()->user();
        $confirm = Invoice::where([
            'id' => $request['id'],
            'id_user' => $user['id'],
        ])->first();
        if ($confirm == null) {
            abort(403);
        }
        $image = $confirm['id_invoice'].'_'.Str::random(8).'.'.$request['payment_image']->getClientOriginalExtension();
        $confirm->update([
            'payment_image' => $image
        ]);
        $request->file('payment_image')->storeAs('public/payment/'.$confirm['id_invoice'], $image);
        return redirect()->back()->with('success', 'Berhasil mengupload bukti pembayaran !');
    }

}
