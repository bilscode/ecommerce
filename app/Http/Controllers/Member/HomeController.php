<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Cart;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $items = Item::where("name", "like", "%{$request['q']}%")->get();
        } else {
            $items = Item::all();
        }
        $data['items'] = $items;
        $data['count'] = count($items);
        return view('member/home/index')->with('data' ,$data);
    }

    public function show($id)
    {
        $data = [];
        $items = Item::findOrFail($id);
        $data['items'] = $items;
        return view('member/home/detail')->with('data' ,$data);
    }

    public function addCart($id, Request $request)
    {
        if (!Auth::check()){
            return redirect()->route('login');
        }
        if(auth()->user()->role === 'ADMIN') {
            return redirect()->back()->with('danger', 'Adminstrator tidak dapat menambahkan keranjang !');
        }
        $data = [];
        $user = auth()->user();
        $cartDistinct = Cart::where(['id_user' => $user['id'],'id_item' => $id]);
        $data['id_user'] = $user['id'];
        $data['id_item'] = $id;
        $request->validate([
            'jumlah' => 'nullable|numeric|min:1',
        ]);
        $total = $request['jumlah'] == null ? 1 : $request['jumlah'];
        if ($cartDistinct->count() == 1) {
            $addTotal = $cartDistinct->first();
            $total = $addTotal['total'] + $total;
            $item = Item::find($id);
            if ($item['stock'] < $total) {
                return redirect()->back()->with('danger', 'Tidak dapat menambahkan ke keranjang dikarenakan stok tidak cukup !');
            }
            $addTotal->update(['total' => $total]);
        } else {
            $data['total'] = $total;
            $cart = Cart::create($data);
        }
        return redirect()->back()->with('success', 'Berhasil menambahkan keranjang !');
    }
}
