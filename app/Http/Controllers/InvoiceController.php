<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helpers;
use App\Models\Invoice;
use App\Models\Order;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['MEMBER','ADMIN'])) {
                return $next($request);
            }
        });
    }

    public function index($id)
    {
        $data = [];
        $invoice = Invoice::with('users')->where('id_invoice', $id)->first();
        $order = Order::where('id_invoice', $id)->get();
        $data['invoice'] = $invoice;
        $data['order'] = $order;
        return view('invoice/index')->with('data' ,$data);
    }

    public function preview($id)
    {
        $data = [];
        $invoice = Invoice::with('users')->where('id_invoice', $id)->first();
        $order = Order::where('id_invoice', $id)->get();
        $data['invoice'] = $invoice;
        $data['order'] = $order;
        return view('invoice/preview')->with('data' ,$data);
    }
}
