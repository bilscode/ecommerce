<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\User;

class PengirimanConttoller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['ADMIN'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        $data = [];
        $waiting = Invoice::where(['status' => 'PENDING'])->latest()->get();
        $process = Invoice::where(['status' => 'PROCESS'])->latest()->get();
        $delivery = Invoice::where(['status' => 'DELIVERY'])->latest()->get();
        $done = Invoice::where(['status' => 'DONE'])->latest()->get();
        $data['waiting'] = $waiting;
        $data['process'] = $process;
        $data['delivery'] = $delivery;
        $data['done'] = $done;
        $data['c_waiting'] = count($waiting);
        $data['c_process'] = count($process);
        $data['c_delivery'] = count($delivery);
        $data['c_done'] = count($done);
        return view('admin/pengiriman/index')->with('data' ,$data);
    }

    public function confirm(Request $request)
    {
        $user = auth()->user();
        $confirm = Invoice::where([
            'id' => $request['id'],
        ])->first();
        if ($confirm == null) {
            abort(403);
        }
        $confirm->update(['status' => 'DELIVERY']);
        $passData['status'] = 'sedang dalam tahap pengiriman oleh kurir';
        $passData['id_invoice'] = $confirm['id_invoice'];
        User::find($confirm['id_user'])->notify(new \App\Notifications\SendUpdateProgressNotifications($passData));
        return redirect()->back()->with('success', 'Berhasil mengubah status pesanan !');
    }

    public function payment(Request $request)
    {
        $user = auth()->user();
        $confirm = Invoice::where([
            'id' => $request['id'],
        ])->first();
        if ($confirm == null) {
            abort(403);
        }
        $confirm->update(['status' => 'PROCESS']);
        $passData['status'] = 'sudah dikonfirmasi, dan akan segera diproses';
        $passData['id_invoice'] = $confirm['id_invoice'];
        User::find($confirm['id_user'])->notify(new \App\Notifications\SendUpdateProgressNotifications($passData));
        return redirect()->back()->with('success', 'Berhasil mengubah status pesanan !');
    }
}
