<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['ADMIN'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        return view('admin/home/index');
    }
}
