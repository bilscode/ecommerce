<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Helpers\Helpers;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use Illuminate\Support\Facades\Storage;

class BarangController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
        $this->middleware(function ($request, $next) {
            if (Helpers::checkRole(['ADMIN'])) {
                return $next($request);
            }
        });
    }

    public function index()
    {
        $data = [];
        $items = Item::latest()->get();
        $data['items'] = $items;
        return view('admin/barang/index')->with('data' ,$data);
    }
    public function show($id)
    {
        $data = [];
        $items = Item::findOrFail($id);
        $data['items'] = $items;
        return view('admin/barang/edit')->with('data' ,$data);;
    }
    public function create(CreateItemRequest $request)
    {
        $data = [];
        $image = Str::lower(str_replace(' ', '_',$request['name'])).'.'.$request['image']->getClientOriginalExtension();
        $data['name'] = Str::title($request['name']);
        $data['image'] = $image;
        $data['price'] = $request['price'];
        $data['stock'] = $request['stock'];
        $data['description'] = $request['description'];
        $items = Item::create($data);
        $request->file('image')->storeAs('public/barang/'.$items['id'], $data['image']);
        return redirect()->back()->with('success', 'Berhasil menambahkan barang !');
    }
    public function update(UpdateItemRequest $request, $id)
    {
        $data = [];
        $items = Item::findOrFail($id);
        $data['name'] = Str::title($request['name']);
        $data['price'] = $request['price'];
        $data['stock'] = $request['stock'];
        $data['description'] = $request['description'];
        if ($request['image'] !== null) {
            Storage::delete('public/barang/'.$items['id'].'/'.$items['image']);
            $image = Str::lower(str_replace(' ', '_',$request['name'])).'.'.$request['image']->getClientOriginalExtension();
            $data['image'] = $image;
            $request->file('image')->storeAs('public/barang/'.$items['id'], $data['image']);
        }
        $update = $items->update($data);
        return redirect()->back()->with('success', 'Berhasil merubah barang !');
    }
    public function destroy(Request $request)
    {
        $items = Item::findOrFail($request['id']);
        Storage::deleteDirectory('public/barang/'.$items['id']);
        $items->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus barang !');
    }
}
