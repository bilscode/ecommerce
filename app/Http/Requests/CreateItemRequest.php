<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'image' => ['required', 'image', 'dimensions:width=400,height=400', 'mimes:png,jpeg,jpg'],
            'price' => ['required', 'numeric', 'min:0'],
            'stock' => ['required', 'numeric', 'min:0'],
            'description' => ['required', 'string', 'max:255', 'min:20'],
        ];
    }

    public function attributes()
    {
        return[
            'name' => 'nama barang', 
            'image' => 'gambar', 
            'price' => 'harga', 
            'stock' => 'stok', 
            'description' => 'deskripsi', 
        ];
    }
}
