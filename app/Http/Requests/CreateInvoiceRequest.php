<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Rules\PhoneNumberRule;

class CreateInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_method' => ['required', 'string', 'max:255', 'in:COD,BANK_BNI,BANK_BCA,BANK_BRI,BANK_MANDIRI'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', new PhoneNumberRule],
            'address' => ['required', 'string', 'min:10'],
            'zip_code' => ['required', 'numeric', 'digits:5'],
        ];
    }

    public function attributes()
    {
        return[
            'id_invoice' => 'id_invoice',
            'name' => 'nama penerima',
            'phone' => 'nomor handphone',
            'address' => 'alamat pengiriman',
            'zip_code' => 'kode pos',
        ];
    }
}
