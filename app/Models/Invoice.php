<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded = [];
    
    public function users()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }
}
