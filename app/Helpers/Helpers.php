<?php 
namespace App\Helpers;

use Carbon\Carbon;
use Request;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Order;

class Helpers {

    public static function checkRole($roles){
        $granted = false;
        foreach ($roles as $role) {
            if(auth()->user()->role == $role){
                $granted = true;
            }
        }
        if ($granted == false) {
            abort(401);
        }
        return true;
    }

    public static function formatDate($date, $full = false){
        if ($date) {
            $dt = Carbon::parse($date);
            if ($full) {
                return Helpers::IndonesianFormatDate($dt->format('d m Y H:i'));
            } else {
                return Helpers::IndonesianFormatDate($dt->format('d m Y'));
            }
        } else {
            return '-';
        }
    }
    
    public static function IndonesianFormatDate($date)
    {
        $month = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];
        $explode = explode(' ', $date);
        if (count($explode) === 4) {
            return $explode[0].' '.$month[(int)$explode[1]].' '.$explode[2].' '.$explode[3] ;
        }
        return $explode[0].' '.$month[(int)$explode[1]].' '.$explode[2] ;
    }

    public static function formatCurrency($number = 0, $unit = '', $isSuffixUnit = false, $decimal = 0){
        if ($isSuffixUnit) {
            return number_format($number, $decimal, ',', '.').' '.$unit;
        } else {
            return $unit.' '.number_format($number, $decimal, ',', '.');
        }
    }

    public static function checkUrlForMenu($path){
        if(Request::segment(2) == $path){
            echo "active";
        }
    }

    public static function countCart()
    {
        $user = auth()->user();
        $countCart = Cart::where('id_user', $user['id'])->count();
        return $countCart;
    }

    public static function listCart()
    {
        $cartList = [];
        $user = auth()->user();
        $cart = Cart::where('id_user', $user['id'])->latest()->get();
        for ($i=0; $i < $cart->count(); $i++) { 
            $items = Item::where('id', $cart[$i]['id_item'])->first();
            $cartList[$i]['id'] = $items['id'];
            $cartList[$i]['name'] = $items['name'];
            $cartList[$i]['image'] = $items['image'];
            $cartList[$i]['price'] = $items['price'];
            $cartList[$i]['description'] = $items['description'];
            $cartList[$i]['total'] = $cart[$i]['total'];
            $cartList[$i]['stock'] = $items['stock'];
        }
        return $cartList;
    }

    public static function summaryPrice()
    {
        $cartList = [];
        $summary = [];
        $summaryPrice = 0;
        $user = auth()->user();
        $cart = Cart::where('id_user', $user['id'])->latest()->get();
        for ($i=0; $i < $cart->count(); $i++) { 
            $items = Item::where('id', $cart[$i]['id_item'])->where('stock', '<>' ,'0')->first();
            $cartList[$i]['price'] = $items['price'];
            $cartList[$i]['total'] = $cart[$i]['total'];
            $summary[] = $cartList[$i]['price'] * $cartList[$i]['total'];
        }
        $summaryPrice = array_sum($summary);
        return $summaryPrice;
    }

    public static function totalProduct()
    {
        $totalProduct = [];
        $total = 0;
        $user = auth()->user();
        $cart = Cart::where('id_user', $user['id'])->latest()->get();
        for ($i=0; $i < $cart->count(); $i++) { 
            $items = Item::where('id', $cart[$i]['id_item'])->where('stock', '<>' ,'0')->first();
            $totalProduct[$i]['id'] = $items['id'];
        }
        $total = count($totalProduct);
        return $total;
    }

    public static function totalOrder($invoice = null)
    {
        $totalOrder = [];
        $total = 0;
        $order = Order::where('id_invoice', $invoice)->latest()->count();
        $total = $order;
        return $total;
    }

    public static function generateIdInvoice()
    {
        $timestamp = now()->timestamp;
        $invoiceID = "INV$timestamp";
        return $invoiceID;
    }
}