<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register'  => true,
    'verify'    => true,
]);

Route::get('/', 'Member\HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profil', 'ProfileController@index')->name('profil');
Route::post('/profil', 'ProfileController@update')->name('profil.update');
Route::post('/profil/update-password', 'ProfileController@updatePassword')->name('profil.password');

Route::get('/invoice/{id}', 'InvoiceController@index')->name('invoiceIndex');
Route::get('/invoice/print/{id}', 'InvoiceController@preview')->name('invoicePreview');

Route::group(['prefix' => '/admin'], function () {
    //Home controller
    Route::get('/', 'Administrator\HomeController@index')->name('adminDashboard');

    //Barang Controller
    Route::get('/barang', 'Administrator\BarangController@index')->name('adminBarang');
    Route::post('/barang', 'Administrator\BarangController@create')->name('adminBarang.create');
    Route::get('/barang/{id}', 'Administrator\BarangController@show')->name('adminBarang.show');
    Route::post('/barang/{id}', 'Administrator\BarangController@update')->name('adminBarang.update');
    Route::post('/barang/hapus/all', 'Administrator\BarangController@destroy')->name('adminBarang.destroy');

    //Status Pengiriman
    Route::get('/status-pengiriman', 'Administrator\PengirimanConttoller@index')->name('adminPengiriman');
    Route::post('/status-pengiriman/confirm', 'Administrator\PengirimanConttoller@confirm')->name('adminPengiriman.confirm');
    Route::post('/status-pengiriman/payment', 'Administrator\PengirimanConttoller@payment')->name('adminPengiriman.payment');
});

Route::group(['prefix' => '/member'], function () {
    //Home controller
    Route::get('/', 'Member\HomeController@index')->name('memberHome');
    Route::get('/detail/{id}', 'Member\HomeController@show')->name('memberHome.detail');
    Route::get('/add-cart/{id}', 'Member\HomeController@addCart')->name('adminDashboard.addCart');

    //Kerangjang controller
    Route::get('/keranjang', 'Member\KeranjangController@index')->name('memberKeranjang');
    Route::get('/keranjang/save/{id}', 'Member\KeranjangController@save')->name('memberKeranjang.save');
    Route::get('/keranjang/delete/{id}', 'Member\KeranjangController@delete')->name('memberKeranjang.delete');

    //Pembayaran controller
    Route::get('/pembayaran', 'Member\PembayaranController@index')->name('memberPembayaran');
    Route::post('/pembayaran', 'Member\PembayaranController@create')->name('memberPembayaran.save');

    //Pengiriman controller
    Route::get('/status-pengiriman', 'Member\PengirimanController@index')->name('memberPengiriman');
    Route::post('/status-pengiriman/konfirmasi', 'Member\PengirimanController@confirm')->name('memberPengiriman.confirm');
    Route::post('/status-pengiriman/pembayaran', 'Member\PengirimanController@payment')->name('memberPengiriman.payment');
});
